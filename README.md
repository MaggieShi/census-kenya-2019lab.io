# census-kenya-2019.gitlab.io

Display preliminary data from the [census](https://www.knbs.or.ke/?wpdmpro=2019-kenya-population-and-housing-census-volume-i-population-by-county-and-sub-county#) using [eCharts](http://echarts.apache.org/)